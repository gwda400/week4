app.controller("NavController",['$http', function($http){
    this.pages = content;
	var nav = this;
	nav.remoteData = [];
	this.idEntry = 'sample';
	
	$http.get('/js/sample.JSON')
		.success(function(data){
			nav.remoteData = data;
			nav.idEntry = nav.remoteData.entry.id;
			console.log(nav.remoteData.entry.id)
		});
	
	this.toggleActive = function (i) {
        angular.forEach(this.pages, function (p) {
            p.active = false;
        });
        this.pages[i].active = true;
    }
}]);

app.controller("GalleryController",['$scope', function($scope){
	$scope.title = 'Gallery Images';
}]);

app.directive('contactForm', function () {
    return {
        restrict: 'E',
        templateUrl:'contact.html',
		controller: function(){
			this.title = 'Image Gallery';
			this.imgUrl = 'img/sample.jpg'
		},
		controllerAs: 'contactCtrl'
    }

});

var content = [
	{
	    title: "Angular Example",
	    description: 'This example is a quick exercise to illustrate how dynamic content can be added to a page. To start the process, lets hit the navbar above',
	    active: true
	},
	{
	    title: "About",
	    description: 'The about description',
	    active: false
	},
	{
	    title: "Contact",
	    description: 'Contact Information',
	    active: false
	},
];